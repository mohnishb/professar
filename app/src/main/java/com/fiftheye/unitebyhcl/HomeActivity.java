package com.fiftheye.unitebyhcl;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    Button mButton;
    int CAMERA_PERMISSION_CODE = 23;
    private static final String TAG = "HomeActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mButton = (Button) findViewById(R.id.btnARFlow);


        mButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        permissionCheckFlow();

                    }
                });
    }

    public void permissionCheckFlow() {
        //First checking if the app is already having the permission
        if (isCameraAllowed()) {
            //getCoordinates();
            movetoARActivity();
        } else {
            //If the app has not the permission then asking for the permission
            requestCameraPermission();
        }
    }

    public void movetoARActivity(){
        Intent intent;
        intent = new Intent(HomeActivity.this, SplashActivity.class);
        startActivity(intent);
        finish();
    }


    //We are calling this method to check the permission status
    private boolean isCameraAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    //Requesting permission
    public void requestCameraPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.CAMERA)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
            Toast.makeText(this, "Permission is ESSENTIAL to proceed.", Toast.LENGTH_LONG).show();
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA},
                CAMERA_PERMISSION_CODE);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == CAMERA_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                movetoARActivity();
                return;

            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Sorry you need the permission to proceed", Toast.LENGTH_LONG).show();
                permissionCheckFlow();
            }
        }
    }
}
