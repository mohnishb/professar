package com.fiftheye.unitebyhcl;

import android.content.Intent;
import android.os.Bundle;

import eu.kudan.kudan.ARAPIKey;
import eu.kudan.kudan.ARActivity;
import eu.kudan.kudan.ARImageNode;
import eu.kudan.kudan.ARImageTrackable;
import eu.kudan.kudan.ARImageTracker;
import eu.kudan.kudan.ARVideoNode;
import eu.kudan.kudan.ARVideoTexture;

public class MainActivity extends ARActivity {

    private static final String TAG = "MainActivity";

    @Override
    public void setup()
    {
        super.setup();

        /*Example 1 : Code Augmentation*/

        // AR Content to be set up here
        // Initialise the image trackable and load the image.
        ARImageTrackable imageTrackable = new ARImageTrackable("Quick Sort Marker");
        imageTrackable.loadFromAsset("QuickSortInPlace.png");

        // Get the single instance of the image tracker.
        ARImageTracker imageTracker = ARImageTracker.getInstance();

        //Add the image trackable to the image tracker.
        imageTracker.addTrackable(imageTrackable);

        // Initialise the image node with our image
        ARVideoTexture tex = new ARVideoTexture();
        tex.loadFromAsset("quicksort.mp4");
        ARVideoNode imageNode = new ARVideoNode(tex);
        imageNode.setScale(2f,2f,2f);

        // Add the image node as a child of the trackable's world
        imageTrackable.getWorld().addChild(imageNode);


        /*Example 2 : History Augmentation*/


        /*Example 3 : Physics Augmentation*/


        /*Example 4 : Geography/Culture Augmentation*/

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);

    // Example of a call to a native method
//    TextView tv = (TextView) findViewById(R.id.sample_text);
//    tv.setText(stringFromJNI());
        ARAPIKey key = ARAPIKey.getInstance();
        //ENTER your key here
        key.setAPIKey("");
    }
//
//    @Override
//    public void onBackPressed()
//    {
//        super.onBackPressed();
//        startActivity(new Intent(MainActivity.this, HomeActivity.class));
//        finish();
//    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }
}
